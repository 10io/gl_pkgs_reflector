## :ferris_wheel: GitLab Package Registry Reflector

What is this?

This is the demo of https://gitlab.com/10io/gl_paremi. Have a look there for the context and usage.

## :gear: Configuration

This project holds the import configuration to copy packages from npmjs.org to https://gitlab.com/10io/pkgs/-/packages. That project is our target Package Registry.

## :bar-chart: Results

Want to see where the action happens? Proceed to the [pipelines](https://gitlab.com/10io/gl_pkgs_reflector/-/pipelines) of this project.
